<?php

/**
 * Created by PhpStorm.
 * User: Matt_
 * Date: 12/10/2016
 * Time: 11:04
 */

namespace UPEC\Controllers;

use Interop\Container\ContainerInterface;

class Controller
{

    protected $ci;

    public function __construct(ContainerInterface $ci)
    {
        $this->ci = $ci;
    }

    public function __get($name)
    {
        if ($this->ci->has($name)) {
            return $this->ci->get($name);
        }
    }

    public function checkRole($reqRole)
    {
        switch ($reqRole) {
            case "Super":
                if ($this->user['role'] == "Super") {
                    return true;
                } else {
                    return false;
                }
            case "Admin":
                if ($this->user['role'] == "Super" || $this->user['role'] == "Admin") {
                    return true;
                } else {
                    return false;
                }
            case "Standard":
                if ($this->user['role'] == 'Standard' || $this->user['role'] == "Super" || $this->user['role'] == "Admin") {
                    return true;
                } else {
                    return false;
                }
            default:  $this->user['role'] == $reqRole; // false;

        }

    }

    public function isSuper() {
        return $this->checkRole('Super');
    }

    public function isAdmin() {
        return $this->checkRole('Admin');
    }
    
    public function isStandard() {
        return $this->checkROle('Standard');
    }

}
