<?php

namespace UPEC\Controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use UPEC\Models\AdminModel;

class AdminController extends Controller
{

    protected $adminModel;

    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->adminModel = new AdminModel($ci->db, $ci->logger);
    }

    public function index(Request $req, Response $res, $args)
    {
        return null;
    }













    public function validUser($user) {

        $filter = [
            'username' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/^[\w\d_.]{4,}$/"
                ]
            ],
            'password' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/^.{6,}$/"
                ]
            ],
            'role' => ['filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => '/(?:super|admin|standard)/'
                ]
            ]
        ];

        $params = filter_var_array($user, $filter);
        foreach ($params as $val) {
            if (empty($val)) {
                return false;
            }
        }
        return true;
    }

       public function validEvent($event) {
        $filter = [
            'startdate' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/"
                ]
            ],
            'finishdate' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/"
                ]
            ],
            'status' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/(?:o|c)/"
                ]
            ],
        ];
        $params = filter_var_array($event, $filter);
        foreach ($params as $val) {
            if (empty($val)) {
                return false;
            }
        }
        return true;
    }


    public function validIdType($idtype) {
        $filter = [
            'name' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/^(\s)*[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*(\s)*$/"
                ]
            ],
        ];

        $params = filter_var_array($idtype, $filter);
        foreach ($params as $val) {
            if (empty($val)) {
                return false;
            }
        }
        return true;
    }



}
