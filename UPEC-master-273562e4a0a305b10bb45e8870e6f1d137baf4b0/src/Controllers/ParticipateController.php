<?php

namespace UPEC\Controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use UPEC\Models\ParticipateModel;

class ParticipateController extends Controller {

    protected $participateModel;

    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->participateModel = new ParticipateModel($ci->db, $ci->logger);
    }

    public function index(Request $req, Response $res, $args)
    {
        return null;
    }

    //PARTICIPANTS
    public function addParticipant(Request $req, Response $res, $args) {
        if(!$this->isSuper()|| !$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $temp = $req->getParsedBody();
        $success = $this->participateModel->addParticipant($temp['eid'], $temp['pid'], $this->user['uid']);
        if ($success) {
            return $res
                ->withJson(["Success" => ["text" => "Participant added"]], 201)
                ->withHeader('Location', '/admin/events/{eid}/participants');
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function deleteParticipant(Request $req, Response $res, $args) {
        if(!$this->isSuper()|| !$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }

        $part = $this->participateModel->getOneParticipant($args['pid'], $args['eid']);
        if ($part == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => "No participant $args[pid] found at event $args[eid]"]], 404);
            // ['error' => $this->error->getJsonMessage(20) ]
        }else {
            $success = $this->participateModel->deleteParticipant($args['eid'], $args['pid']);
        }
        if ($success) {
            return $res->withJson(["Success" => ["text" => "Participant deleted"]], 200);
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function getParticipants(Request $req, Response $res, $args){
        if(!$this->isSuper()|| !$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $parts = $this->participateModel->getParticipants($args['eid']);
        if ($parts == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $parts]);
        }
    }

    public function getOneParticipant(Request $req, Response $res, $args){
        if(!$this->isSuper()|| !$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $part = $this->participateModel->getOneParticipant($args['pid'], $args['eid']);
        if ($part == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => "No participant $args[pid] found at event $args[eid]"]], 404);
            // ['error' => $this->error->getJsonMessage(20) ]
        } else {
            return $res->withJson(['data' => $part]);
        }
    }

}