<?php

namespace UPEC\Controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use UPEC\Models\UsersModel;

class UsersController extends Controller
{

    protected $userModel;

    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->userModel = new UsersModel($ci->db, $ci->logger);
    }

    public function index(Request $req, Response $res, $args)
    {
        return null;
    }

//##change all user functions to look for super authorization only##
//USER
    public function addUser(Request $req, Response $res, $args)
    {
        try {
            if(!$this->isSuper()){
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
            }
            $temp = $req->getParsedBody();
            if ($this->validUser($temp)) {
                $success = $this->userModel->addUser($temp['username'], password_hash($temp['password'], PASSWORD_DEFAULT), $temp['pid'], $temp['role']);
                if ($success == null) {
                    return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
                } else {
                    return $res
                        ->withJson(["success" => ["text" => "User added"]], 201)
                        ->withHeader('Location', '/admin/users');
                }
            } else {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            }
        } catch (PDOException $e) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }


    public function deleteUser(Request $req, Response $res, $args)
    {
        if(!$this->isSuper()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $user = $this->userModel->getUserById($args['uid']);
        if ($user == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error, User does not exist']], 404);
        }else {
            $success = $this->userModel->deleteUser($args['uid']);
        }
        if ($success) {
            return $res->withJson(["success" => ["text" => "User deleted"]], 200);
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function editUser(Request $req, Response $res, $args)
    {
        if(!$this->isSuper()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $user = $this->userModel->getUserById($args['uid']);
        if ($user == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error, User does not exist']], 404);
        }
        //$temp = $req->getParsedBody();
        $patch = \gamringer\JSONPatch\Patch::fromJSON($req->getBody());
        $uid = $user['uid'];
        $patch->apply($user);
        unset($user['uid']);
        if ($this->validUser($user) || true) {
            $success = $this->userModel->editUser($user['username'], password_hash($user['password'], PASSWORD_DEFAULT), $user['pid'], $user['role'], $args['uid']);
            if ($success) {
                return $res->withJson(["success" => ["text" => "User Updated"]], 200);
            } else {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            }
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
}


    public function getUsers(Request $req, Response $res, $args)
    {
        if(!$this->isSuper()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $users = $this->userModel->getUsers();
        if ($users == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $users]);
        }
    }

    public function getUserById(Request $req, Response $res, $args)
    {
        if(!$this->isSuper()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $user = $this->userModel->getUserById($args['uid']);
        if ($user == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $user]);
        }
    }

    public function validUser($user) {

        $filter = [
            'username' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/^[\w\d_.]{4,}$/"
                ]
            ],
            'password' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/^.{6,}$/"
                ]
            ],
            'role' => ['filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => '/(?:Super|Admin|Standard)/'
                ]
            ]
        ];

        $params = filter_var_array($user, $filter);
        foreach ($params as $val) {
            if (empty($val)) {
                return false;
            }
        }
        return true;
    }
}