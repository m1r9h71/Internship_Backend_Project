<?php

namespace UPEC\Controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use UPEC\Models\CategoryModel;
use UPEC\Controllers\AdminController;

class CategoryController extends Controller {

    protected $categoryModel;

    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->categoryModel = new CategoryModel($ci->db, $ci->logger);
    }

    public function index(Request $req, Response $res, $args)
    {
        return null;
    }
    //##change all category functions to need super authorization only##
    public function getCategories(Request $req, Response $res, $args)
    {
        if(!$this->isStandard()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $cats = $this->categoryModel->getCategories();
        if ($cats == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $cats]);
        }
    }

    public function addCategory(Request $req, Response $res, $args) {
        if(!$this->isSuper()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $temp = $req->getParsedBody();
        if ($this->validCategory($temp)) {
            $success = $this->categoryModel->addCategory($temp['name']);
            if ($success) {
                return $res
                    ->withJson(["Success" => ["text" => "Category added"]], 201)
                    ->withHeader('Location', '/admin/categories');
            } else {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            }
        } else {
            return $res->withJson(['error' => ['code' => 1,'message' => 'Error']], 404);
        }
    }

    public function deleteCategory(Request $req, Response $res, $args) {
        if(!$this->isSuper()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $cat = $this->categoryModel->getCategoryById($args['cid']);
        if ($cat == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }else {
            $success = $this->categoryModel->deleteCategory($args['cid']);
        }
        if ($success) {
            return $res->withJson(["Success" => ["text" => "Category deleted"]], 200);
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function editCategory(Request $req, Response $res, $args) {
        if(!$this->isSuper()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $cat = $this->categoryModel->getCategoryById($args['cid']);
        if ($cat == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
        $temp = $req->getParsedBody();
        if ($this->validIdType($temp)) {
            $success = $this->categoryModel->editCategory($args['cid'], $temp['name']);
            if ($success) {
                return $res->withJson(["Success" => ["text" => "Category updated"]], 200);
            } else {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            }
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function getCategoryById(Request $req, Response $res, $args)
    {
        if(!$this->isStandard()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $cat = $this->categoryModel->getCategoryById($args['cid']);
        if ($cat == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $cat]);
        }
    }

    public function validCategory($category) {
        $filter = [
            'name' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/^(\s)*[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*(\s)*$/"
                ]
            ],
        ];

        $params = filter_var_array($category, $filter);
        foreach ($params as $val) {
            if (empty($val)) {
                return false;
            }
        }
        return true;
    }
}