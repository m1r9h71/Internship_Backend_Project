<?php

namespace UPEC\Controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use UPEC\Models\AttendModel;

class AttendController extends Controller {

    protected $attendModel;

    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->attendModel = new AttendModel($ci->db, $ci->logger);
    }

    public function index(Request $req, Response $res, $args)
    {
        return null;
    }

    public function addAttendee(Request $req, Response $res, $args)
    {

        $temp = $req->getParsedBody();

        if ($this->validAttendee($temp)) {
            $success = $this->attendModel->addAttendee($temp['adate'], $temp['pid'], $args['eid'], $this->user['uid']);
            if ($success == null) {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            } else {
                return $res
                    ->withJson(["success" => ["text" => "Attendee added"]], 201)
                    ->withHeader('Location', '/admin/{eid:\d+}/attendees');

            }
        }
    }
    //##all attendee functions change to standard - admina and super as well ##
    //ATTENDEE
    //##Are you allowed to delete attendees? check-in system (maybe super role)
    public function deleteAttendee(Request $req, Response $res, $args) {
        //check if attendee exists!
        $oneAttend = $this->attendModel->getOneAttendee($args['aid']);
        if ($oneAttend == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }else {
            $success = $this->attendModel->deleteAttendee($args['aid']);
        }
        if ($success) {
            return $res->withJson(["success" => ["text" => "Attendee deleted"]], 200);
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    //##Are you allowed to edit this information? maybe only super role
    public function editAttendee(Request $req, Response $res, $args) {
        $attend = $this->attendModel->getOneAttendee($args['aid']);
        if ($attend == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
        //$temp = $req->getParsedBody();
        //if (validAttendee($temp)) {
        $patch = \gamringer\JSONPatch\Patch::fromJSON($req->getBody());
        $aid = $attend['aid'];
        $patch->apply($attend);
        unset($attend['aid']);
        if ($this->validAttendee($attend) || true) {
            $success = $this->attendModel->editAttendee($attend['adate'], $attend['pid'], $attend['eid'], $this->user['uid'], $args['aid']);
            if ($success) {
                return $res->withJson(["success" => ["text" => "Attendee updated"]], 200);
                //$patch = UPEC\Patch::fromJSON(file_get_contents(__DIR__ . '/../admin/attendees/{aid}'));
               // {op: 'replace', path: '/admin/attendees/{aid}', value: <value>}
            } else {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            }
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function getEventAttendees(Request $req, Response $res, $args) {

        $attends = $this->attendModel->getEventattendees($args['eid']);
        if ($attends == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $attends]);
        }
    }

    public function getAllAttendeesEvents(Request $req, Response $res, $args) {

        $attevent = $this->attendModel->getAllAttendeesEvents($args['aid']);
        if ($attevent == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $attevent]);
        }
    }

    public function getOneAttendee(Request $req, Respsnse $res, $args) {
        $oneAttend = $this->attendModel->getOneAttendee($args['aid']);
        if ($oneAttend == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }else {
            return $res->withJson(['data' => $oneAttend]);
        }
    }


    public function validAttendee($attendee)
    {

        $data['adate'] = $attendee['adate'];
        $data['pid'] = $attendee['pid'];

        $filter = [
            'pid' => ['filter' => FILTER_VALIDATE_INT],
            'adate' => ['filter' => FILTER_CALLBACK,
                'options' => array($this, "validateDate")
            ]
        ];

        $params = filter_var_array($data, $filter);
        foreach ($params as $val) {
            if (empty($val)) {
                return false;
            }
        }
        return true;
    }

    function validateDate($date, $format = 'Y-m-d H:i:s')
    { //delete
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }


}