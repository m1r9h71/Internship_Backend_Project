<?php

namespace UPEC\Controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use UPEC\Models\IdentifyModel;

class IdentifyController extends Controller {

    protected $identifyModel;

    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->identifyModel = new IdentifyModel($ci->db, $ci->logger);
    }

    public function index(Request $req, Response $res, $args)
    {
        return null;
    }

    //IDENTIFIER
    //##Could be user from idtypes instead of using directly model (if not, NOT NEEDED)
    public function addIdentifier(Request $req, Response $res, $args) {
        if(!$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $temp = $req->getParsedBody();
        $success = $this->identifyModel->addIdentifier($temp['id'], $temp['pid'], $temp['tid'], $this->user['uid']);
        if ($success) {
            return $res
                ->withJson(["success" => ["text" => "Identifier added"]], 201)
                ->withHeader('Location', '/admin/identifies');
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function deleteIdentifier(Request $req, Response $res, $args) {
        if(!$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $ident = $this->identifyModel->getIdentifierById($args['id']);
        if ($ident == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }else {
            $success = $this->identifyModel->deleteIdentifier($args['id']);
        }
        if ($success) {
            return $res->withJson(["success" => ["text" => "Identifier deleted"]], 200);
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function getIdentifier(Request $req, Response $res, $args) {
        if (!$this->isStandard()) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $idents = $this->identifyModel->getIdentifiers();
        if ($idents == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $idents]);
        }
    }

    public function getIdentifierByPeople(Request $req, Response $res, $args) {
        if (!$this->isStandard()) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $idents = $this->identifyModel->getIdentifierByPeople($args['pid']);
        if ($idents == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $idents]);
        }
    }

    public function getIdentifierById(Request $req, Response $res, $args) {
        if (!$this->isStandard()) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $ident = $this->identifyModel->getIdentifierById($args['id']);
        if ($ident == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $ident]);
        }
    }
}