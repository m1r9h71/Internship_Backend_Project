<?php

namespace UPEC\Controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use UPEC\Models\EventModel;

class EventController extends Controller
{

    protected $eventModel;

    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->eventModel = new EventModel($ci->db, $ci->logger);
    }

    public function index(Request $req, Response $res, $args)
    {
        return null;
    }

    //EVENTS
    //##change all event functions to super and admin NOTE: gets are all 3 ##
    public function addEvent(Request $req, Response $res, $args)
    {

        if(!$this->isSuper()|| !$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $temp = $req->getParsedBody();

        if ($this->validEvent($temp)) {
            $success = $this->eventModel->addEvent($temp['title'], $temp['description'], $temp['place'], $temp['startdate'], $temp['finishdate'], $temp['status'], $temp['cid']);
            if ($success) {
                return $res
                    ->withJson(["success" => ["text" => "Event added"]], 201)
                    ->withHeader('Location', '/admin/events');
            } else {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            }
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function deleteEvent(Request $req, Response $res, $args)
    {
        if(!$this->isSuper()|| !$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }

        $event = $this->eventModel->getOneEvent($args['eid']);
        if ($event == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error, event does not exist']], 404);
        }else {
            $success = $this->eventModel->deleteEvent($args['eid']);
        }
        if ($success) {
            return $res->withJson(["success" => ["text" => "Event deleted"]], 200);
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function editEvent(Request $req, Response $res, $args)
    {
        if(!$this->isSuper()|| !$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $event = $this->eventModel->getOneEvent($args['eid']);
        if ($event == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error, event does not exist']], 404);
        }
       // $temp = $req->getParsedBody();
        $patch = \gamringer\JSONPatch\Patch::fromJSON($req->getBody());
        $eid = $event['eid'];
        $patch->apply($event);
        unset($event['eid']);
        if ($this->validEvent($event) || true) {
            $success = $this->eventModel->editEvent($event['title'], $event['description'], $event['place'], $event['startdate'], $event['finishdate'], $event['status'], $event['cid'], $args['eid']);
            if ($success) {
                return $res->withJson(["success" => ["text" => "Event updated"]], 200);
            } else {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            }
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function addParticipantToEvent(Request $req, Response $res, $args)
    {
        //participants can be added multiple times as they may leave and come back in.
        $temp = $req->getParsedBody();
       if(true) {
            $success = $this->eventModel->addParticipantToEvent($temp['eid'], $temp['pid'], $this->user[uid]);
            if($success) {
                return $res
                    ->withJson(["success" => ["text" => "Participant added to event"]], 201)
                    ->withHeader('Location', '/admin/events/{eid}/participants');

            }else {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            }
        }
   }


    public function getEvents(Request $req, Response $res, $args)
    {

        $events = $this->eventModel->getEvents();
        if ($events == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $events]);
        }
    }

    public function getOneEvent(Request $req, Response $res, $args)
    {

        $event = $this->eventModel->getOneEvent($args['eid']);
        if ($event == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error, event does not exist']], 404);
        } else {
            return $res->withJson(['data' => $event]);
        }
    }





    public function addAttendee(Request $req, Response $res, $args)
    {

        $temp = $req->getParsedBody();

        if ($this->validAttendee($temp)) {
            $success = $this->eventModel->addAttendee($temp['adate'], $temp['pid'], $args['eid'], $this->user['uid']);
            if ($success == null) {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            } else {
                return $res
                    ->withJson(["success" => ["text" => "Attendee added"]], 201)
                    ->withHeader('Location', '/events/{eid:\d+}/attendees');

            }
        }
    }

    public function addAttendeeList(Request $req, Response $res, $args)
    {
        $temp = $req->getParsedBody();
        foreach ($temp['attendee'] as $row) {
            if (validAttendee($row)) {
                $success = $this->eventModel->addAttendee($row['adate'], $row['pid'], $row['eid'], $row['uid']);
                if(!$success){
                    $error = "Error adding: " + $row['pid'] + "to" + $row['eid'];
                }
            } else {
                $error = "Error adding: " + $row['pid'] + "to" + $row['eid'];
            }
        }
        if (!empty($error)) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(["success" => ["text" => "Attendees added"]], 201);
        }
    }

    public function validAttendee($attendee)
    {

        $data['adate'] = $attendee['adate'];
        $data['pid'] = $attendee['pid'];

        $filter = [
            'pid' => ['filter' => FILTER_VALIDATE_INT],
            'adate' => ['filter' => FILTER_CALLBACK,
                'options' => array($this, "validateDate")
            ]
        ];

        $params = filter_var_array($data, $filter);
        foreach ($params as $val) {
            if (empty($val)) {
                return false;
            }
        }
        return true;
    }

    function validateDate($date, $format = 'Y-m-d H:i:s')
    { //delete
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }


    public function validEvent($event)
    {
        $filter = [
            'startdate' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/"
                ]
            ],
            'finishdate' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/"
                ]
            ],
            'status' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/(?:o|c|O|C)/"
                ]
            ],
        ];
        $params = filter_var_array($event, $filter);
        foreach ($params as $val) {
            if (empty($val)) {
                return false;
            }
        }
        return true;
    }

}