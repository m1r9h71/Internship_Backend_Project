<?php

namespace UPEC\Controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use UPEC\Models\IdtypesModel;

class IdtypesController extends Controller {

    protected $idtypesModel;

    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->idtypesModel = new IdtypesModel($ci->db, $ci->logger);
    }

    public function index(Request $req, Response $res, $args)
    {
        return null;
    }

    //IDTYPES
    //##change all Idtypes to super Authentication only
    public function addIdtype(Request $req, Response $res, $args) {
        if(!$this->isSuper()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $temp = $req->getParsedBody();
        if ($this->validIdType($temp)) {
            $success = $this->idtypesModel->addIdtype($temp['name']);
            if ($success) {
                    return $res
                        ->withJson(["Success" => ["text" => "ID Type added"]], 201)
                        ->withHeader('Location', '/admin/idtypes');
                } else {
                    $this->idtypesModel->deleteIdentifier($temp['tid']);
                    return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
                }
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function deleteIdtype(Request $req, Response $res, $args) {
        if(!$this->isSuper()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 404);
        }
        $idtypes = $this->idtypesModel->getIdtypesById($args['tid']);
        if ($idtypes == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }else {
            $success = $this->idtypesModel->deleteIdtype($args['tid']);
        }

        if ($success) {
            return $res->withJson(["Success" => ["text" => "ID Type deleted"]], 200);
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function editIdtype(Request $req, Response $res, $args) {
        if(!$this->isSuper()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $idtypes = $this->idtypesModel->getIdtypesById($args['tid']);
        if ($idtypes == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
        $patch = \gamringer\JSONPatch\Patch::fromJSON($req->getBody());
        $tid = $idtypes['tid'];
    //    var_dump($idtypes);
   //     die();
        $patch->apply($idtypes);
        unset($idtypes['tid']);
       //$idtypes = $req->getParsedBody();
      if ($this->validIdType($idtypes) || true) {
            $success = $this->idtypesModel->editIdtype($idtypes['name'],$args['tid']);
            if ($success) {
                return $res->withJson(["Success" => ["text" => "ID Type updated"]], 200);
            } else {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            }
       } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function getIdtypes(Request $req, Response $res, $args) {
        if(!$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $idtypes = $this->idtypesModel->getIdtypes();
        if ($idtypes == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $idtypes]);
        }
    }

    public function getIdtypesById(Request $req, Response $res, $args) {
        if(!$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $idtypes = $this->idtypesModel->getIdtypesById($args['tid']);
        if ($idtypes == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $idtypes]);
        }
    }

    public function validIdType($idtype) {
        $filter = [
            'name' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/^(\s)*[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*(\s)*$/"
                ]
            ],
        ];

        $params = filter_var_array($idtype, $filter);
        foreach ($params as $val) {
            if (empty($val)) {
                return false;
            }
        }
        return true;
    }
}