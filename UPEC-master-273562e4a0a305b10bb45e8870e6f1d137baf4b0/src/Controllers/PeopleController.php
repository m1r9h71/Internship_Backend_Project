<?php



namespace UPEC\Controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use UPEC\Models\PeopleModel;

class PeopleController extends Controller {

    protected $peopleModel;

    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->peopleModel = new PeopleModel($ci->db, $ci->logger);
    }

    public function index(Request $req, Response $res, $args)
    {
        return null;
    }

//PERSON
//##change all people functions to super and admin only except get functions that can be done by all 3##
    public function addPerson(Request $req, Response $res, $args) {
        if(!$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $temp = $req->getParsedBody();
        if ($this->validPerson($temp)) {
            $success = $this->peopleModel->addPerson($temp['fname'], $temp['lname']);
            if ($success) {
                return $res
                    ->withJson(["success" => ["text" => "Person added"]], 201)
                    ->withHeader('Location', '/admin/people');
            } else {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
            }
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function deletePerson(Request $req, Response $res, $args) {
        if(!$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $person = $this->peopleModel->getPersonById($args['pid']);
        if ($person == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error, Person does not exist']], 404);
        }else {
            $success = $this->peopleModel->deletePerson($args['pid']);
        }

        if ($success) {
            return $res->withJson(["success" => ["text" => "Person deleted"]], 200);
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error', 404]]);
        }
    }


    public function editPerson(Request $req, Response $res, $args) {
        if(!$this->isAdmin()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $person = $this->peopleModel->getPersonById($args['pid']);
        if ($person == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error, Person does not exist']], 404);
        }
        //$temp = $req->getParsedBody();
        $patch = \gamringer\JSONPatch\Patch::fromJSON($req->getBody());
        $pid = $person['pid'];
        $patch->apply($person);
        unset($person['pid']);
        if ($this->validPerson($person) || true) {
            $success = $this->peopleModel->editPerson($args['pid'], $person['fname'], $person['lname']);
            if ($success) {
                return $res->withJson(["success" => ["text" => "Person updated"]], 200);
            } else {
                return $res->withJson(['error' => ['code' => 1, 'message' => 'Error', 404]]);
            }
        } else {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        }
    }

    public function getPeople(Request $req, Response $res, $args) {
        if(!$this->isStandard()){
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $people = $this->peopleModel->getPeople();
        if ($people == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $people]);
        }
    }

    public function getPersonById(Request $req, Response $res, $args) {
        if (!$this->isStandard()) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Access Error']], 401);
        }
        $person = $this->peopleModel->getPersonById($args['pid']);
        if ($person == null) {
            return $res->withJson(['error' => ['code' => 1, 'message' => 'Error']], 404);
        } else {
            return $res->withJson(['data' => $person]);
        }
    }


    public function validPerson($prsn) {
        $filter = [
            'fname' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/^(\s)*[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*(\s)*$/"
                ]
            ],
            'lname' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    'regexp' => "/^(\s)*[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*(\s)*$/"
                ]
            ],
        ];
        $params = filter_var_array($prsn, $filter);
        foreach ($params as $val) {
            if (empty($val)) {
                return false;
            }
        }
        return true;
    }




}