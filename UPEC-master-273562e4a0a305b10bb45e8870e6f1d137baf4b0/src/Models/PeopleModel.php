<?php

namespace UPEC\Models;

use PDO;
use PDOException;
class PeopleModel extends Model {

    //PERSON
    public function addPerson($fname, $lname) {
        $sql = 'insert into people (fname, lname) values (:fname, :lname)';
        try {
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(['fname' => $fname, 'lname' => $lname]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('PEOPLE: not added');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function deletePerson($pid) {
        $sql = 'delete from people where pid = :pid';
        try {
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["pid" => $pid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('PEOPLE: not deleted');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function editPerson($pid, $fname, $lname) {
        $sql = 'update people set fname = :fname, lname = :lname where pid = :pid';
        try {
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["pid" => $pid, "fname" => $fname, "lname" => $lname]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('PEOPLE: not updated');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getPeople() {
        $sql = 'select * from people order by fname';
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->execute();
            $people = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($people)) {
                return $people;
            } else {
                $this->logger->addInfo('PEOPLE: empty');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getPersonById($pid) {
        $sql = 'select * from people where pid = :pid';
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->execute(["pid" => $pid]);
            $person = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($person)) {
                return $person;
            } else {
                $this->logger->addInfo('PEOPLE: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getPersonByName($fname, $lname) {
        $sql = 'select * from people where fname = :fname and lname = :lname';
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->execute(["fname" => $fname, "lname" => $lname]);
            $person = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($person)) {
                return $person;
            } else {
                $this->logger->addInfo('PEOPLE: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }



}