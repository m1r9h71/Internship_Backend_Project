<?php

/**
 * Created by PhpStorm.
 * User: Matt_
 * Date: 12/10/2016
 * Time: 11:08
 */

namespace UPEC\Models;

class Model {

    protected $db;
    protected $logger;

    public function __construct($db, $logger) {
        $this->db = $db;
        $this->logger = $logger;
    }

}
