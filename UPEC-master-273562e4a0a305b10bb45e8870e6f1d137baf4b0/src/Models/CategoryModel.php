<?php

namespace UPEC\Models;

use PDO;
use PDOException;
class CategoryModel extends  Model {

    public function addCategory($name) {
        try {
            $sql = 'insert into category(name) values (:name)';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["name" => $name]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('CATEGORY: not added ');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function deleteCategory($cid) {
        try {
            $sql = 'delete from category where cid = :cid';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["cid" => $cid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('CATEGORY: not deleted');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function editCategory($cid, $name) {
        try {
            $sql = 'update category set name = :name where cid = :cid';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["name" => $name, "cid" => $cid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('CATEGORY: not updated');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getCategories() {
        try {
            $sql = 'select * from category order by name';
            $stmt = $this->db->prepare($sql);
            $stmt->execute();
            $cats = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($cats)) {
                return $cats;
            } else {
                $this->logger->addInfo('CATEGORIES: empty');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getCategoryById($cid) {
        try {
            $sql = 'select * from category where cid = :cid';
            $stmt = $this->db->prepare($sql);
            $stmt->execute(["cid" => $cid]);
            $cat = $stmt->fetch(PDO::FETCH_ASSOC);

            if (!empty($cat)) {
                return $cat;
            } else {
                $this->logger->addInfo('CATEGORY: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }


}