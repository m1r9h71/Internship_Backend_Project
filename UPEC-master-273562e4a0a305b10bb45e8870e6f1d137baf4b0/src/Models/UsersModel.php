<?php

namespace UPEC\Models;

use PDO;
use PDOException;
class UsersModel extends Model {

    //USER
    public function addUser($user, $pass, $pid, $role) {
        try {
            $sql = 'insert into users(username, password, pid, role) values (:user, :pass, :pid, :role)';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["user" => $user, "pass" => $pass, "pid" => $pid, "role" => $role]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('USER: not added ');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function editUser($user, $pass, $pid, $role, $uid) {
        try {
            $SQL = "update users set username = :user, password = :pass, pid = :pid, role = :role where uid = :uid";
            $stmt = $this->db->prepare($SQL);
            $success = $stmt->execute(["user" => $user, "pass" => $pass, "pid" => $pid, "role" => $role, "uid" => $uid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('USER: not updated');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function deleteUser($uid) {
        try {
            $SQL = "delete from users where uid = :uid";
            $stmt = $this->db->prepare($SQL);
            $success = $stmt->execute(["uid" => $uid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('USER: not deleted');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getUsers() {
        try {
            $SQL = "SELECT * FROM users order by username";
            $stmt = $this->db->prepare($SQL);
            $stmt->execute();
            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($users)) {
                return $users;
            } else {
                $this->logger->addInfo('USERS: empty');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getUserById($uid) {
        try {
            $SQL = "SELECT * FROM users where uid = :uid";
            $stmt = $this->db->prepare($SQL);
            $stmt->execute(["uid" => $uid]);
            $users = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($users)) {
                return $users;
            } else {
                $this->logger->addInfo('USER: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }
    
    public function getUserByName($username) {
        try {
            $SQL = "SELECT * FROM users where username = :username";
            $stmt = $this->db->prepare($SQL);
            $stmt->execute(["username" => $username]);
            $users = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($users)) {
                return $users;
            } else {
                $this->logger->addInfo('USER: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }
}