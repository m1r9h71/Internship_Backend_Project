<?php

namespace UPEC\Models;

use PDO;
use PDOException;

class AttendModel extends Model
{

    //ATTENDEE
    public function addAttendee($adate, $pid, $eid, $uid)
    {
        try {
            $sql = 'insert into attend(adate, pid, eid, uid) values (:adate, :pid, :eid, :uid)';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["adate" => $adate, "pid" => $pid, "eid" => $eid, "uid" => $uid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('ATTENDEE: not added ');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function deleteAttendee($aid)
    {
        $sql = 'delete from attend where aid = :aid';
        try {
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["aid" => $aid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('ATTEND: not deleted');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function editAttendee($adate, $aid, $eid, $pid, $uid)
    {
        $sql = 'update attend set adate = :adate, eid = :eid, pid = :pid, uid = :uid where aid = :aid';
        try {
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute([
                "adate" => $adate,
                "aid" => $aid,
                "eid" => $eid,
                "pid" => $pid,
                "uid" => $uid
            ]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('EVENT: not updated');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getEventAttendees($eid)
    {
        $sql = 'SELECT title, description, place, startdate, finishdate, status, fname, lname, adate FROM `attend` 
                JOIN events ON attend.eid = events.eid join people on attend.pid = people.pid WHERE events.eid = :eid';
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->execute(["eid" => $eid]);
            $attendees = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($attendees)) {
                return $attendees;
            } else {
                $this->logger->addInfo('ATTEND/EVENTS: not found');
                return null;
            }
        } catch (PDOExecute $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getAllAttendeesEvents($aid)
    {
        $sql = 'select fname, lname, description, startdate, finishdate, place, status, title,username from attend
          join events on attend.eid = events.eid join people on attend.pid = people.pid join users on attend.uid = users.uid 
          where aid =  :aid';
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->execute(["aid" => $aid]);
            $events = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($events)) {
                return $events;
            } else {
                $this->logger->addInfo('EVENTS/ATTEND: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }


    public function getOneAttendee($aid)
    {
        try {
            $sql = 'select * from attend where aid = :aid';
            $stmt = $this->db->prepare($sql);
            $stmt->execute(["aid" => $aid]);
            $attend = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($attend)) {
                return $attend;
            } else {
                $this->logger->addInfo('Attendee: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

}