<?php

namespace UPEC\Models;

use PDO;
use PDOException;

class EventModel extends Model {

    //EVENTS
    public function addEvent($title, $description, $place, $startdate, $finishdate, $status, $cid) {
        try {
            $sql = 'insert into events(title, description, place, startdate, finishdate, status, cid) values
        (:title, :description, :place, :startdate, :finishdate, :status, :cid)';

            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(['title' => $title, 'description' => $description, 'place' => $place, 'startdate' => $startdate, 'finishdate' => $finishdate,
                'status' => $status, 'cid' => $cid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('EVENTS: not added');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function deleteEvent($eid) {
        try {
            $sql = 'delete from events where eid = :eid';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["eid" => $eid]);
            if ($success) {
                return true;
                ;
            } else {
                $this->logger->addInfo('EVENTS: not deleted ');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function editEvent($title, $description, $place, $startdate, $finishdate, $status, $cid, $eid) {
        try {

            $sql = 'update events set cid = :cid, description = :description, finishdate = :finishdate, place = :place,
             startdate = :startdate, status = :status, title = :title where eid = :eid';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute([
                "cid" => $cid,
                "description" => $description,
                "eid" => $eid,
                "finishdate" => $finishdate,
                "place" => $place,
                "startdate" => $startdate,
                "status" => $status,
                "title" => $title
            ]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('EVENTS: not updated ');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addInfo('PDO Error', $e->getMessage());
            return null;
        }
    }
    public function addParticipantToEvent($eid, $pid, $uid)
    {
        try{
            $sql = 'insert into participate(eid, pid, uid) values (:eid, :pid, :uid)';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute([
                'eid' => $eid,
                'pid' => $pid,
                'uid' => $uid
            ]);
            if ($success) {
                return true;
            }else {
                $this->logger->addInfo('Participant: not added');
                return null;
            }
        }catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
        }
    }

    public function getEvents() {
        try {
            $SQL = "SELECT * FROM events order by title";
            $stmt = $this->db->query($SQL);
            $stmt->execute();
            $events = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($events)) {
                return $events;
            } else {
                $this->logger->addInfo('EVENTS: empty');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getOneEvent($eid) {
        try {
            $sql = 'select * from events where eid = :eid';
            $stmt = $this->db->prepare($sql);
            //$stmt->bindValue(':id', $req->getAttribute("id"), PDO::PARAM_INT);
            $stmt->execute(["eid" => $eid]);
            //$stmt->execute();
            $event = $stmt->fetch(PDO::FETCH_ASSOC);

            if (!empty($event)) {
                return $event;
            } else {
                $this->logger->addInfo('EVENT: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }





    public function addAttendee($adate, $pid, $eid, $uid) {
        try {
            $sql = 'insert into attend(adate, pid, eid, uid) values (:adate, :pid, :eid, :uid)';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["adate" => $adate, "pid" => $pid, "eid" => $eid, "uid" => $uid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('ATTENDEE: not added ');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

}
