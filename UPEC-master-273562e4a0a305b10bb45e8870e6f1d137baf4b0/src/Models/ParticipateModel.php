<?php

namespace UPEC\Models;

use PDO;
use PDOException;

class ParticipateModel extends Model {

    //PARTICIPANTS
    public function addParticipant($eid, $pid, $uid) {
        try {
            $sql = 'insert into participate(pid, eid, uid) values (:pid, :eid, :uid)';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute([
                'eid' => $eid,
                'pid' => $pid,
                'uid' => $uid
            ]);
            if ($success) {
                return true;
            } else {
                $this->logger->addinfo('PARTICIPANT: not added ');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', [$e->getMessage()]);
            return null;
        }
    }

    public function editParticipant($eid, $uid, $pid, $oeid, $opid) {
        try {
            $sql = 'update participate set eid = :eid, uid = :uid, pid = :pid where eid = :oeid and pid = :opid';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute([
                "eid" => $eid,
                "uid" => $uid,
                "pid" => $pid,
                "oeid" => $oeid,
                "opid" => $opid
            ]);
            if ($success) {
                return true;
            } else {
                $this->logger->addinfo('PARTICIPANT: not updated');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function deleteParticipant($eid, $pid) {
        try {
            $sql = 'delete from participate where eid = :eid and pid = :pid';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["eid" => $eid, "pid" => $pid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addinfo('PARTICIPANT: not deleted ');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getParticipants($eid) {
        try {//including uid so that we can keep a track of who checks in each participant!
            $sql = 'select uid, fname, lname, title, description, startdate, finishdate, status from people  join participate p1 using (pid) join events using (eid) where events.eid = :eid';
            $stmt = $this->db->prepare($sql);
            $stmt->execute(["eid" => $eid]);
            $parts = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (true) {
                return $parts;
            } else {
                $this->logger->addInfo('PARTICIPANTS: empty');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getOneParticipant($pid, $eid) {
        try {
            $sql = 'select uid, fname, lname, title, description, startdate, finishdate, status from people  join participate p1 using (pid) join events using (eid) where pid = :pid and eid = :eid';
            $stmt = $this->db->prepare($sql);
            $stmt->execute(["pid" => $pid, "eid" => $eid]);
            $part = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($part)) {
                return $part;
            } else {
                $this->logger->addInfo('PARTICIPANT: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

}