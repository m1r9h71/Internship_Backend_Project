<?php

namespace UPEC\Models;

use PDO;
use PDOException;
class IdentifyModel extends Model {

    //IDENTIFIER
    public function addIdentifier($id, $pid, $tid, $uid) {
        $sql = 'insert into identify(id, pid, tid, uid)values(:id, :pid, :tid, :uid)';
        try {
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(['id' => $id, 'pid' => $pid, 'tid' => $tid, 'uid' => $uid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('IDENTIFY: not added');
                return null;
            }
            return $success;
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function deleteIdentifier($id) {
        $sql = 'delete from identify where id = :id';
        try {
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["id" => $id]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('IDENTIFY: not deleted');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getIdentifiers() {
        $sql = 'select id, fname, lname, idtypes.name as idtype from identify left join people on identify.pid = people.pid left join idtypes on identify.tid = idtypes.tid order by fname';
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->execute();
            $ids = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($ids)) {
                return $ids;
            } else {
                $this->logger->addInfo('IDENTIFY: empty');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getIdentifierById($id) {
        $sql = 'select id, people.pid, fname, lname from identify left join people on identify.pid = people.pid where id = :id';
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->execute(["id" => $id]);
            $idt = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($idt)) {
                return $idt;
            } else {
                $this->logger->addInfo('IDENTIFY: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getIdentifierByPeople($pid) {
        $sql = 'select id, fname, lname from identify left join people on identify.pid = people.pid where people.pid = :pid';
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->execute(["pid" => $pid]);
            $id = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($id)) {
                return $id;
            } else {
                $this->logger->addInfo('IDENTIFY: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

}