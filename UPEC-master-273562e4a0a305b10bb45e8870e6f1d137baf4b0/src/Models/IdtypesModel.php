<?php

namespace UPEC\Models;

use PDO;
use PDOException;
class IdtypesModel extends Model {

    //IDTYPES
    public function addIdtype($name) {
        try {
            $sql = 'insert into idtypes(name) values (:name)';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["name" => $name]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('IDTYPE: not added ');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function deleteIdtype($tid) {
        try {
            $sql = 'delete from idtypes where tid = :tid';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["tid" => $tid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('IDTYPE: not deleted');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function editIdtype($tid, $name) {
        try {
            $sql = 'update idtypes set name = :name where tid = :tid';
            $stmt = $this->db->prepare($sql);
            $success = $stmt->execute(["name" => $name, "tid" => $tid]);
            if ($success) {
                return true;
            } else {
                $this->logger->addInfo('IDTYPE: not updated');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getIdtypes() {
        try {
            $sql = 'select * from idtypes order by name';
            $stmt = $this->db->prepare($sql);
            $stmt->execute();
            $types = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($types)) {
                return $types;
            } else {
                $this->logger->addInfo('IDTYPE: empty');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }

    public function getIdtypesById($tid) {
        try {
            $sql = 'select * from idtypes where tid = :tid';
            $stmt = $this->db->prepare($sql);
            $stmt->execute(["tid" => $tid]);
            $types = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($types)) {
                return $types;
            } else {
                $this->logger->addInfo('IDTYPE: not found');
                return null;
            }
        } catch (PDOException $e) {
            $this->logger->addError('PDO Error', $e->getMessage());
            return null;
        }
    }
}