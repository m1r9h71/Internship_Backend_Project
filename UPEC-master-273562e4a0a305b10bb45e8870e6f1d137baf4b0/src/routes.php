<?php
// Routes



//NON-ADMIN
//events
$app->post('/events', "EventController:addEvent");//ok 04-11-2016

$app->delete('/events/{eid:\d+}', "EventController:deleteEvent");//only deletes when UID is 4 or 2! 04-11-16

$app->patch('/events/{eid:\d+}', "EventController:editEvent");//ok 04-11-2016

//$app->post('/events/{eid:\d+}/participants', "EventController:addParticipantToEvent");//ok 04-11-2016

$app->get('/', "EventController:index");

$app->get('/events',"EventController:getEvents");//ok 03-11-2016

$app->get('/events/{eid:\d+}',"EventController:getOneEvent");//ok 03-11-2016

//participants
$app->get('/events/{eid:\d+}/participants',"ParticipateController:getParticipants");//ok 03-11-16

$app->get('/events/{eid:\d+}/participants/{pid:\d+}',"ParticipateController:getOneParticipant");//ok 03-11-16

//attendees
$app->post('/events/{eid:\d+}/attendees', "EventController:addAttendee");//ok 04-11-2016

$app->post('/events/{eid:\d+}/attendees/bulk', "EventController:addAttendeeList");//only calls up addAttendee same as previous function!

//categories
$app->get('/events/categories',"CategoryController:getCategories");//ok 03-11-2016
$app->get('/events/categories/{cid:\d+}',"CategoryController:getCategoryById");// done

//ADMIN
//USER
$app->post('/admin/users', "UsersController:addUser");// done
$app->delete('/admin/users/{uid:\d+}', "UsersController:deleteUser");// done
$app->patch('/admin/users/{uid:\d+}', "UsersController:editUser");// done
$app->get('/admin/users', "UsersController:getUsers");//done
$app->get('/admin/users/{uid:\d+}', "UsersController:getUserById");// done

//PERSON
$app->post('/admin/people', "PeopleController:addPerson");//done
$app->delete('/admin/people/{pid:\d+}', "PeopleController:deletePerson");// done
$app->patch('/admin/people/{pid:\d+}', "PeopleController:editPerson");//done
$app->get('/admin/people', "PeopleController:getPeople");//done
$app->get('/admin/people/{pid:\d+}', "PeopleController:getPersonById");//done

//IDENTIFIER to modify
$app->post('/admin/identifies', "IdentifyController:addIdentifier");// done
$app->delete('/admin/identifies/{id}', "IdentifyController:deleteIdentifier");// done
$app->get('/admin/identifies/people/{pid}', "IdentifyController:getIdentifierByPeople");// done
$app->get('/admin/identifies/{id}', "IdentifyController:getIdentifierById");// FIX get
$app->get('/admin/identifies', "IdentifyController:getIdentifier"); // done


//ATTENDEE /persons/2/identifiers/2
$app->post('/admin/{eid:\d+}/attendees', "AttendController:addAttendee");//ok - in eventmodel
$app->delete('/admin/attendees/{aid}', "AttendController:deleteAttendee");//ok
$app->patch('/admin/attendees/{aid:\d+}', "AttendController:editAttendee");//do we need?
$app->get('/admin/attendees/events/{eid}', "AttendController:getEventAttendees");//ok 03-11-2016
$app->get('/admin/attendees/{aid}', "AttendController:getAllAttendeesEvents");//ok 03-11-2016 - changed to aid

//EVENT
$app->post('/admin/events', "EventController:addEvent");//ok
$app->delete('/admin/events/{eid:\d+}', "EventController:deleteEvent");//ok
$app->patch('/admin/events/{eid:\d+}', "EventController:editEvent");//ok

//PARTICIPANTS
$app->post('/admin/events/{eid}/participants', "ParticipateController:addParticipant");//ok
$app->delete('/admin/participants/{pid:\d+}/events/{eid:\d+}', "ParticipateController:deleteParticipant");//ok

//ID TYPES
$app->post('/admin/idtypes', "IdtypesController:addIdtype");// done
$app->delete('/admin/idtypes/{tid}', "IdtypesController:deleteIdtype");// done
$app->patch('/admin/idtypes/{tid}', "IdtypesController:editIdtype");// done
$app->get('/admin/idtypes', "IdtypesController:getIdtypes");// done
$app->get('/admin/idtypes/{tid}', "IdtypesController:getIdtypesById");// done

//CATEGORIES
$app->post('/admin/categories', "CategoryController:addCategory");// done
$app->delete('/admin/categories/{cid}', "CategoryController:deleteCategory");// done
$app->patch('/admin/categories/{cid}', "CategoryController:editCategory");// done







