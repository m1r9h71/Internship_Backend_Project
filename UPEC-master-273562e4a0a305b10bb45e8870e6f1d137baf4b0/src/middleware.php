<?php

// Application middleware
// e.g: $app->add(new \Slim\Csrf\Guard);
use \Slim\Middleware\HttpBasicAuthentication\PdoAuthenticator;
//use \PDO;

//$pdo = new \PDO("sqlite:/tmp/users.sqlite");

$app->add(new \Slim\Middleware\HttpBasicAuthentication([
    "path" => "/",
    "realm" => "Protected",
    "authenticator" => new PdoAuthenticator([
        //"pdo" => $app->getContainer()->$pdo,
        "pdo" => $app->getContainer()->get('db'),
        "table" => "users",
        "user" => "username",
        "hash" => "password",
            ]),
    "callback" => function ($request, $response, $arguments) use ($app) {
        $um = new \UPEC\Models\UsersModel($app->getContainer()->db,$app->getContainer()->logger);
        $app->getContainer()['user'] = $um->getUserByName($arguments['user']);
        return true;
    },
]));
