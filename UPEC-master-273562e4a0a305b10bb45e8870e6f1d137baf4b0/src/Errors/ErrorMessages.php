<?php
/**
 * Created by PhpStorm.
 * User: Matt_
 * Date: 22/11/2016
 * Time: 15:40
 */

namespace UPEC\Errors;


class ErrorMessages
{
   private static $messages = [
       1  => "General error",
       25 => "No participant at the event",
    ];

   public static function getMessage($errorCode){
       return self::messages[$errorCode];
   }

    public static function getJsonMessage($errorCode){
        return  [ 'code' => $errorCode, 'message' => self::messages[$errorCode] ] ;
    }



}