<?php

// DIC configuration


$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['db'] = function ($c) {
    $settings = $c->get('settings')['pdo'];
    $pdo = new PDO($settings['dsn'], $settings['user'], $settings['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

$container['EventController'] = function ($c) {
    return new \UPEC\Controllers\EventController($c);
};

$container['AdminController'] = function ($c) {
    return new \UPEC\Controllers\AdminController($c);
};

$container['AttendController'] = function ($c) {
    return new \UPEC\Controllers\AttendController($c);
};

$container['CategoryController'] = function ($c) {
    return new \UPEC\Controllers\CategoryController($c);
};

$container['IdentifyController'] = function ($c) {
    return new \UPEC\Controllers\IdentifyController($c);
};

$container['IdtypesController'] = function ($c) {
    return new \UPEC\Controllers\IdtypesController($c);
};

$container['ParticipateController'] = function ($c) {
    return new \UPEC\Controllers\ParticipateController($c);
};

$container['PeopleController'] = function ($c) {
    return new \UPEC\Controllers\PeopleController($c);
};

$container['UsersController'] = function ($c) {
    return new \UPEC\Controllers\UsersController($c);
};




